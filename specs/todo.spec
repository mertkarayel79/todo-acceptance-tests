# ToDo List Application Acceptance Tests

## Add Todo To Empty ToDo List

* Go to ToDo page
* Empty ToDo list
* Write "buy some milk" to text box and click to add button
* Check Todo List contains "buy some milk"

## Add Todo To Full ToDo List
* Go to ToDo page
* Write "buy some milk" to text box and click to add button
* Write "enjoy the assignment" to text box and click to add button
* Check "enjoy the assignment" to Todo List below "buy some milk"

## Mark Todo
* Go to ToDo page
* Write "buy some milk" to text box and click to add button
* Click on "buy some milk" text for mark 
* Check "buy some milk" is marked

## UnMark Todo
* Go to ToDo page
* Write "buy some milk" to text box and click to add button
* Click on "buy some milk" text for mark 
* Click on "buy some milk" text for unmark
* Check "buy some milk" is unmarked

## Delete Todo When Todo List Only Have One Element
* Go to ToDo page
* Write "rest for a while" to text box and click to add button
* Delete "rest for a while" text
* Empty ToDo list

## Delete Todo When List Have More Than One Element 
* Go to ToDo page
* Write "rest for a while" to text box and click to add button
* Write "drink water" to text box and click to add button
* Delete "rest for a while" text
* Check Todo List contains "drink water"