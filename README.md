# Todo App Acceptance Tests
This project contains acceptance test for todo app.

[![demo](https://asciinema.org/a/328053.svg)](https://asciinema.org/a/sqhRuYFEdgQ1mOUtGK55ZzjAy)

## Requirements
- [Java8](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Selenium](https://www.selenium.dev/documentation/en/grid/setting_up_your_own_grid/)
- [Gauge](https://gauge.org/)

## Folder Structure

```
.
├── env                   # Environemnt variables are under this folder
│   ├── default           # Default environment variables
│   ├── firefox           # Environment variables for firefox
│   └── automated-test    # Environment variables for automated-test
├── specs                 # Specs is under this folder
│   ├── todo.spec         # Acceptance criterias for Todo app
├── src/test/java         # All business logic related things are under this folder.
│   ├── driver            # Browser related operations
│   ├── todo              # Acceptance tests are defined under thsi folder
├── .gitlab-ci.yml        # Pipeline file
├── README.md
├── pom.xml
```

## Building The Project

Clone the repository

```
$ git clone https://gitlab.com/mertkarayel79/todo-acceptance-tests.git
$ cd todo-acceptance-tests
```

Run it 
```
$ mvn clean test
$ mvn clean test -Denv=automated-test # for running with environment variable
```

## How To Trigger Acceptance Tests

Acceptance tests are automatically triggered every hour by [Gitlab CI/CD pipeline](.gitlab-ci.yml).

## License

[GNU GENERAL PUBLIC LICENSE](LICENSE)


