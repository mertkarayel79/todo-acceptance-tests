package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.grpc.netty.shaded.io.netty.util.internal.ObjectUtil;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {

    // Get a new WebDriver Instance.
    // There are various implementations for this depending on browser. The required browser can be set as an environment variable.
    // Refer http://getgauge.io/documentation/user/current/managing_environments/README.html
    public static WebDriver getDriver() {

        String browser = System.getenv("BROWSER");
        browser = (browser == null) ? "chrome": browser;

        String seleniumHubIp = System.getenv("SELENIUM_HUB_IP");
        if(!StringUtils.isEmpty(seleniumHubIp)){
            try {
                // Only allowed for firefox...
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                capabilities.setBrowserName(browser);
                capabilities.setCapability("takesScreenshot", "true");
                return new RemoteWebDriver(new URL(seleniumHubIp), capabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        switch (browser) {
            case "IE":
                WebDriverManager.iedriver().setup();
                return new InternetExplorerDriver();
            case "FIREFOX":
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            case "CHROME":
            default:
                WebDriverManager.chromedriver().setup();

	            ChromeOptions options = new ChromeOptions();
	            if ("Y".equalsIgnoreCase(System.getenv("HEADLESS"))) {
	                options.addArguments("--headless");
	                options.addArguments("--disable-gpu");
	            }

	            return new ChromeDriver(options);
        }
    }
}
