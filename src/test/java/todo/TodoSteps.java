package todo;

import com.thoughtworks.gauge.*;
import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

public class TodoSteps {

    private static final String STYLE = "style";
    private static final String EMPTY_STYLE = "";
    private static final String CLASS_TODO = "todo";
    private static final String CLASS_NEW_TODO = "new-todo";
    private static final String CLASS_TODO_ITEM = "todo-item";
    private static final String CLASS_ADD_BUTTON = "add-button";
    private static final String CLASS_DELETE_BUTTON = "delete-button";
    private static final String TEXT_DECORATION = "text-decoration: line-through;";

    private static final WebDriver webDriver = Driver.webDriver;

    @BeforeScenario
    @AfterScenario
    public void beforeScenario() {
        List<WebElement> elements = getWebElementWithClassNameTodoItem();
        clickAllDeleteButton(elements);
    }

    @Step("Go to ToDo page")
    public void goToToDoPage() {
        String app_url = System.getenv("APP_URL");
        webDriver.get(app_url + "/");
        assertThat(webDriver.getTitle()).contains("ToDo");
    }

    @Step("Empty ToDo list")
    public void emptyToDoList() {
        sleep(400);
        assertEquals(0, getWebElementWithClassNameTodoItem().size());
    }

    @Step("Write <text> to text box and click to add button")
    public void writeTextToTextBoxAndClickToAddButton(String text) {
        addTextInsideNewTodo(text);
        clickAddButton();

        sleep(250);

        assertTrue(isExistWebElementsWithClassNameTodo(text));
    }

    @Step("Check <firstText> to Todo List below <secondText>")
    public void checkToTodoListBelow(String firstText, String secondText) {
        List<WebElement> elements = webDriver.findElements(By.className(CLASS_TODO));
        assertEquals(2, elements.size());
        assertEquals(secondText, elements.get(0).getText());
        assertEquals(firstText, elements.get(1).getText());
    }

    @Step("Check Todo List contains <text>")
    public void checkToTodoListBelow(String text) {
        assertTrue(isExistWebElementsWithClassNameTodo(text));
    }

    @Step("Check <text> is marked")
    public void checkTextIsMarked(String text) {
        List<WebElement> elements = webDriver.findElements(By.className(CLASS_TODO));
        WebElement webElement = findWebElementWithGivenText(elements, text);
        assertEquals(webElement.getAttribute(STYLE), TEXT_DECORATION);
    }

    @Step("Check <text> is unmarked")
    public void checkTextIsUnMarked(String text) {
        sleep(100);
        List<WebElement> elements = webDriver.findElements(By.className(CLASS_TODO));
        WebElement webElement = findWebElementWithGivenText(elements, text);
        assertEquals(webElement.getAttribute(STYLE), EMPTY_STYLE);
    }

    @Step("Click on <text> text for mark")
    public void clickOnTextTextForMark(String text) {
        revertMark(text);
    }

    @Step("Click on <text> text for unmark")
    public void clickOnTextTextForUnmark(String text) {
        revertMark(text);
    }

    @Step("Delete <text> text")
    public void deleteTextText(String text) {
        List<WebElement> elements = getWebElementWithClassNameTodoItem();
        WebElement element = findWebElementWithGivenTextWithInTodoClassName(elements, text);
        clickDeleteButton(element);
    }

    private void revertMark(String text) {
        sleep(100);
        List<WebElement> elements = getWebElementsWithClassNameTodo();

        for (WebElement element : elements) {
            if (element.getText().equals(text)) {
                element.click();
            }
        }
    }

    private List<WebElement> getWebElementsWithClassNameTodo() {
        sleep(100);
        return webDriver.findElements(By.className(CLASS_TODO));
    }

    private List<WebElement> getWebElementWithClassNameTodoItem() {
        sleep(100);
        return webDriver.findElements(By.className(CLASS_TODO_ITEM));
    }

    private boolean isExistWebElementsWithClassNameTodo(String text) {
        sleep(100);
        List<WebElement> webElements = webDriver.findElements(By.className(CLASS_TODO));
        for (WebElement element : webElements) {
            if (element.getText().equals(text)) {
                return true;
            }
        }
        return false;
    }

    private WebElement findWebElementWithGivenText(List<WebElement> webElements, String text) {
        sleep(100);
        for (WebElement element : webElements) {
            if (element.getText().equals(text)) {
                return element;
            }
        }
        throw new IllegalStateException();
    }

    private WebElement findWebElementWithGivenTextWithInTodoClassName(List<WebElement> webElements, String text) {
        sleep(100);
        for (WebElement element : webElements) {
            if (element.findElement(By.className(CLASS_TODO)).getText().equals(text)) {
                return element;
            }
        }
        throw new IllegalStateException();
    }

    private void addTextInsideNewTodo(String text) {
        WebElement inputText = webDriver.findElement(By.className(CLASS_NEW_TODO));
        inputText.sendKeys(text);
    }

    private void clickAddButton() {
        WebElement addButton = webDriver.findElement(By.className(CLASS_ADD_BUTTON));
        addButton.click();
    }

    private void clickDeleteButton(WebElement webElement) {
        webElement.findElement(By.className(CLASS_DELETE_BUTTON)).click();
        sleep(100);
    }

    private void clickAllDeleteButton(List<WebElement> webElements) {
        for (WebElement webElement : webElements) {
            clickDeleteButton(webElement);
        }
    }

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}